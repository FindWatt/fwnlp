import pytest
import spacy
import FwNLP

NLP = spacy.load('en')

class TestGetText():
    def test_blank(self):
        nlp_test = FwNLP.get_text(NLP(""))
        assert nlp_test == []
    def test_sentence(self):
        nlp_test = FwNLP.get_text(NLP("This is a test."))
        assert nlp_test == ['This', 'is', 'a', 'test', '.']
    
class TestMergeHyphenated():
    def test_blank(self):
        nlp_test = NLP("")
        nlp_merged = FwNLP.merge_hyphenated(nlp_test)
        assert FwNLP.get_text(nlp_merged) == []
    def test_merged(self):
        nlp_test = NLP("This is a walking-stick.")
        assert FwNLP.get_text(nlp_test) == ['This', 'is', 'a', 'walking','-','stick', '.']
        nlp_merged = FwNLP.merge_hyphenated(nlp_test)
        assert FwNLP.get_text(nlp_merged) == ['This', 'is', 'a', 'walking-stick', '.']

class TestNounPhrases():
    def test_blank(self):
        nlp_test = NLP("")
        nlp_noun_phrases = FwNLP.noun_phrases(nlp_test)
        assert nlp_noun_phrases == []
    def test_phrases(self):
        nlp_test = NLP("This is a noun phrase and so is this second test.")
        nlp_noun_phrases = [FwNLP.get_text(phrase) for phrase in FwNLP.noun_phrases(nlp_test)]
        assert nlp_noun_phrases == [['noun','phrase'], ['second','test']]


#def test_this_works():
#    assert True