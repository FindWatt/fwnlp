# ### General purpose utilities, packaged in different categories ### #

The most frequent kinds of functions in FwUtil are related to working with text (tokenization, cleaning, splitting, replacing, etc) using the module FwText, and counting (frequencies of items in lists) using FwFreq.

Example usage:

```
#!python

from FwUtil.FwFreq import freq
from FwUtil import get_public_ip

l = ["one", "one", "two", "three", "four", "four"]

frequencies = freq(l)

ip = get_public_ip()

```

or

```
#!python

import FwUtil

l = ["one", "one", "two", "three", "four", "four"]

frequencies = FwUtil.FwFreq.freq(l)

ip = FwUtil.get_public_ip()

```