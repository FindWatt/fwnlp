import pyximport; pyximport.install()
try:
    from nlp_phrases import *
except:
    from .nlp_phrases import *

__version__ = "0.0.2"