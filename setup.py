import os, sys

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwNLP/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:-1]

def download_spacy_model():
    # try to check if spacy model is already downloaded and installed
    import spacy
    try:
        o_nlp = spacy.load('en')
        if o_nlp.__dict__.get('path'): return None
    except:
        pass
        
    #print("About to download spacy model...")
    os.system("python -m spacy download en")
    #os.system("pip install https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-1.2.0/en_core_web_sm-1.2.0.tar.gz")
    #this will always download even if model is already present
    #spacy.cli.download('en')
            
class custom_install(install):
    def run(self):
        print("This is a custom installation")
        install.run(self)
        download_spacy_model()

setuptools.setup(
    name="FwNLP",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwnlp",

    author="FindWatt",

    description="NLP Utilities",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt"]},
    py_modules=['FwNLP'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "spacy",
    ],
    cmdclass={'install': custom_install}
)


